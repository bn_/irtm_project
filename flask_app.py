from flask import Flask, render_template, request
from index import index, get_dict_count, get_line_count, process_query, fetch_tweet, unpickle, read_glossary, get_best_results
import logging

app = Flask(__name__)
glossary = read_glossary("/home/max8464/static/english-words")

@app.route('/count')
def count():
    return "Dictionary size: %d<br>Tweets indexed: %d"%(get_dict_count(),get_line_count())
    
def get_to_work():
    logging.getLogger().setLevel(20)
    if get_dict_count()==0:
        #in case of a server restart or app crash
        logging.info("Dictionary empty, fetching from cache")
        unpickle("/home/max8464/static/tweets")
    if get_dict_count()==0:
        logging.info("Dictionary still empty, reindexing")
        index("/home/max8464/static/tweets")

@app.route('/')
def query_form():
    get_to_work()
    return render_template('query.html')

@app.route('/query', methods=['GET'])
def results():
    query_text = request.args.get("query")
    spellcheck = request.args.get("spellcheck")=="yes"
    best_only = request.args.get("best_only")=="yes"
    or_mode = request.args.get("or")=="yes"
    if or_mode:
        mode = "OR"
    else:
        mode = "AND"
    search_terms, result_indices = process_query(query_text, spellcheck, glossary, mode= mode)
    if best_only:
        results = [fetch_tweet(i[0]) for i in get_best_results(search_terms,result_indices)] #query, document_ids, count=10)
    else:
        results = [fetch_tweet(i) for i in result_indices]        
    return render_template('results.html', query = str(search_terms), total = len(results), results = results)


