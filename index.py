# -*- coding: utf-8 -*-
"""
Information Retrieval and Text Mining
Homework exercise 1
"""
import re
import logging
import pickle
import time
import sys
import os
import math
import heapq
# initialize variables
dictionary = {}
postings_lists = [] # a list of lists, used to demonstrate the separation 
                    # between the dictionary and the posting lists
                    # otherwise pointless
tweets = []
# the regular expression pattern will be used for normalization
pattern = re.compile('[\W_]+')

def normalize(word):
    word=pattern.sub('', word)    #strip non-alphanumeric characters
    return word.lower()           #and convert to lowercase

"""
Store a word and the document index in the dictionary
"""
prev_docindex = -1
def index_word(docindex,word,dictionary=dictionary,postings_lists=postings_lists,spellcheck=False):
    logging.getLogger().setLevel(20)
    word_normalized = normalize(word)
    increase_document_frequency = 0
    
    global prev_docindex # to avoid counting the same document multiple times
    if docindex != prev_docindex:
        increase_document_frequency = 1
        prev_docindex = docindex
        
    if len(word_normalized)==0:  # normalization converts garbage to empty lines
        return
    if word_normalized in dictionary:
        list_size, list_index, document_frequency = dictionary[word_normalized]
        if docindex not in postings_lists[list_index]:
            # append the document index to an existing postings list
            postings_lists[list_index].append(docindex)
            dictionary[word_normalized][0]=list_size+1  #increment length
            dictionary[word_normalized][2]=document_frequency+increase_document_frequency  #increment document_frequency if the document is new
            
    else:
        # create a new postings list and put the document index there
        postings_lists.append([docindex]) # add a list to the list of lists
        dictionary[word_normalized]=[1,len(postings_lists)-1,1] #store length, reference and document frequency

"""
Returns an (index, text) tuple from the given line
"""
def split_line(line):
    split = line.replace("\n","").split("\t")
    tweet_index = split[1]
    tweet_text = split[-1]
    return (tweet_index, tweet_text)

"""
Index a file.
"""
def index(filename, limit=-1, chunking=10000, spellcheck=False):
    logging.getLogger().setLevel(20)
    debug_start_time = time.time()
    debug_iteration_time = time.time()
    temp_dict = {}
    temp_lists = []
    with open(filename, encoding="utf8") as f:
        for line_index, line in enumerate(f):
            (tweet_index, tweet_text) = split_line(line)
            # to make code shorter, we store all tweets in memory
            tweets.append((tweet_index,tweet_text)) 
            words = tweet_text.split(" ")
            for word in words:
                index_word(line_index,word,temp_dict,temp_lists,spellcheck=spellcheck)
            if line_index%10000==0 and line_index>0:     # status message
                debug_duration = time.time() - debug_iteration_time
                debug_iteration_time = time.time()
                debug_rate = 10000/max(debug_duration,1)
                logging.info("Indexed #%d tweets at %d tweets per second."%(line_index,debug_rate))
                #compacting dictionaries to improve performance
                if len(temp_dict) > chunking and chunking > -1:
                    for k in temp_dict:
                        if k in dictionary:
                            list_size, list_index, document_frequency = dictionary[k]
                            list_size_t, list_index_t, document_frequency_t = temp_dict[k]
                            postings_lists[list_index] += temp_lists[list_index_t] 
                            postings_lists[list_index].sort()
                            dictionary[k][0]=dictionary[k][0]+temp_dict[k][0] #increase length
                            dictionary[k][2]=dictionary[k][2]+temp_dict[k][2]
                        else:
                            list_size_t, list_index_t, document_frequency_t = temp_dict[k]
                            postings_lists.append(temp_lists[list_index_t].copy())
                            dictionary[k] = [len(temp_lists[list_index_t]),len(postings_lists)-1,1]
                    temp_dict = {}
                    temp_lists = []
            if line_index >= limit and limit>-1:
                logging.info("Indexing limit of %d lines reached."%limit)
                break
        logging.info("Indexing complete, total time: %d s"%(time.time()-debug_start_time))
    THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
    with open(os.path.join(THIS_FOLDER, 'dict.bin'), 'wb') as f:
        pickle.dump((filename,dictionary),f)
    with open(os.path.join(THIS_FOLDER, 'lists.bin'), 'wb') as f:
        pickle.dump(postings_lists,f)

"""
Quick retrieval of cached data.
"""
def unpickle(filename="/home/max8464/static/tweets"):
    THIS_FOLDER = os.path.dirname(os.path.abspath(__file__))
    with open(os.path.join(THIS_FOLDER, 'dict.bin'), 'rb') as f:
        global dictionary
        (nothing,dictionary)=pickle.load(f)
    with open(os.path.join(THIS_FOLDER, 'lists.bin'), 'rb') as f:
        global postings_lists
        postings_lists=pickle.load(f)
    global tweets
    tweets = []
    with open(filename, encoding="utf8") as f:
        for line_index, line in enumerate(f):
            (tweet_index, tweet_text) = split_line(line)
            tweets.append((tweet_index,tweet_text)) 
        

"""
Query the inverted index for terms.
Please note this method now takes a list as argument, and works through it recursively.
"""
def query(terms=[], mode="AND"):
    if len(terms)==0:
        return []
    elif len(terms)==1:
        # if only one term is present, return the corresponding posting list
        normalized_term = normalize(terms[0])
        if normalized_term!="" and normalized_term in dictionary:
            list_index = dictionary[normalized_term][1]
            return postings_lists[list_index]
        else:
            return []
    else:
        postings1 = query([terms[0]]) # the first term
        postings2 = query(terms[1:]) # and the rest of the list. This feels so Haskell-y.
        # initialize the return list and iterators
        if mode=="AND":
            results = []
            iter1 = iter(postings1)
            iter2 = iter(postings2)
            # this is the conjunction algorithm from the lecture
            try:
                i=next(iter1)
                j=next(iter2)
                while True:
                    if i==j:
                        # match found
                        results.append(i)
                        i=next(iter1)
                        j=next(iter2)
                    elif i<j:
                        i=next(iter1)
                    elif j<i:
                        j=next(iter2)
            except StopIteration:
                # we've finished scanning one of the lists
                return results
        elif mode=="OR":
            results = list(set(postings1 + postings2))
            return results

def fetch_tweet(i):
    return (tweets[i][0],tweets[i][1])

def print_results(indices):
    print("%d results."%(len(indices)))
    for i in indices:
        # again for the sake of brevity, we output the selected tweets from memory
        print("id %s: %s"%(tweets[i][0],tweets[i][1]))
    
def get_line_count():
    return len(tweets)
    
def get_dict_count():
    return len(dictionary)

def read_glossary(filename):
    words = []
    with open(filename, encoding="utf8") as f:
        words = f.read().splitlines()
    return words

"""
Just another Levenshtein distance function.
"""
import numpy as np
def levenshtein(string1, string2, limit=-1):
    l1 = len(string1)
    l2 = len(string2)
    if limit>-1 and abs(l1-l2)>=limit:
        return limit
    if l1 < l2:
        return levenshtein(string2, string1, limit)
    if l2 == 0:
        return l1
    #initialize matrix        
    m = np.zeros([l1+1,l2+1])
    for i in range(l1+1):
        m[i,0]=i
    for j in range(1,l2+1):
        m[0,j]=j
    #main cycle
    for i in range(1,l1+1):
        col_min = max(l1,l2)
        for j in range(1,l2+1):
            if string1[i-1]==string2[j-1]:
                sub_cost=0
            else:
                sub_cost=1
            m[i,j]=min([m[i-1,j]+1,m[i,j-1]+1,m[i-1,j-1]+sub_cost])
            if m[i,j]<col_min:
                col_min=m[i,j] # abort if the minimum edit distance has become too large
        if col_min>=limit and limit>-1:
            return col_min
    return int(m[l1,l2])

"""
If a word is misspelled, iterate over the glossary until the best match 
(i.e. term with the shortest edit distance) is found.
"""
def correct_spelling(word, glossary,limit=3):
    if word in glossary:
        return word
    best_match=(word,limit)
    for entry in glossary:
        lev = levenshtein(word,entry,limit)
        if lev < best_match[1]:
            best_match=(entry,lev)
    logging.info("Spelling correction: %s -> %s"%(word,best_match))
    return best_match[0]

"""
This function is used to break down a string into search terms, 
normalize them, spell check (if necessary), and start the query.
"""
def process_query(query_text, spellcheck=False, glossary=[], mode="AND"):
    terms = [normalize(term) for term in query_text.split(" ") if len(term)>0]
    if spellcheck:
        search_terms=[correct_spelling(term,glossary,3) for term in terms]
    else:
        search_terms=terms
    return (search_terms,query(search_terms, mode=mode))
    

"""
Converts a list of words into a vector of tf-idf weights
"""    
def words_to_vector(words):
    return_dict = {}
    tf_dict = {}
    # first, compute document frequencies
    for word in words:
        if word in tf_dict:
            tf_dict[word]+=1
        else:
            tf_dict[word]=1
    # then, calculate TF-IDF weights
    for word in tf_dict:
        # this is the TF-IDF formula
        # len(tweets) is N
        # column [2] of the dictionary stores the document frequency
        if word in dictionary:
            return_dict[word] = (1+math.log10(tf_dict[word])) * math.log10(len(tweets)/(dictionary[word][2]))
        else:
            # avoiding division by zero (or key error, in this specific implementation)
            return_dict[word] = (1+math.log10(tf_dict[word])) * math.log10(len(tweets))
    return return_dict

"""
Converts a string to a list of normalized words
"""
def tokenize(line):
    return [normalize(x) for x in line.split(" ")]
    
"""
Helper function for cosine_similarity
"""
def vector_dot_product(v1, v2):
    total = 0
    for word in v1:
        if word in v2:
            total += v1[word]*v2[word]
    return total

"""
Another helper function for cosine_similarity
"""    
def vector_magnitude(v):
    return math.sqrt(sum([v[word]*v[word] for word in v]))
    
"""
Calculate cosine similarity between two vectors.
v1, v2 are dictionaries of the form {'word': weight}
"""    
def cosine_similarity(v1, v2):
    return vector_dot_product(v1, v2)/(vector_magnitude(v1)*vector_magnitude(v2))
    
def get_best_results(query_words, document_ids, count=10):
    q_vector = words_to_vector(query_words)
    d_vectors = [(i,words_to_vector(tokenize(tweets[i][1]))) for i in document_ids]
    return heapq.nlargest(count,d_vectors, key=lambda x: cosine_similarity(x[1],q_vector))
        

"""
This program can be used from the command line interface.
"""    
if __name__ == "__main__":
    glossary=read_glossary("english-words")
    # index the tweets file, without length limit, merging the dictionaries after every 10000 entries
    index("z:/tweets", -1, 10000)    
    while True:
        q = input("Please enter your query: ")
        if q=="":
            break
        else:
            print_results(process_query(q,True,glossary)[1])